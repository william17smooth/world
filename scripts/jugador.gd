extends Area2D

const HORIZONTAL=0
const ABAJO=1
const ARRIBA=2


export (Array,SpriteFrames) var animaciones

export var retardo=0.3
var ruta=[]
var puedo_mover=true
var direccion=ABAJO
func _ready():
	pass

func _physics_process(delta):
	if ruta.size()>0:
		if puedo_mover:
			$spr.animation="walk"
			puedo_mover=false
			var dir = ruta[0]*16+Vector2(8,8)
			ruta.remove(0)
			mover(dir)
	else:
		if puedo_mover:
			$spr.animation="idle"

func mover(dir):
	var direc=(dir-position).normalized()
	if direc==Vector2.UP:
		direccion=ARRIBA
	elif direc==Vector2.DOWN:
		direccion=ABAJO
	elif direc==Vector2.RIGHT or direc==Vector2.LEFT:
		direccion=HORIZONTAL
		if (dir-position).x<0:
			flip(true)
		else:
			flip(false)

	$spr.frames=animaciones[direccion]

	var tiempo=float((dir-position).length()*retardo/16)
	
	var tween = get_node("tween")
	tween.interpolate_property(self, "position",
			position, dir, tiempo,
			Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.start()
	yield($tween,"tween_completed")
	puedo_mover=true

func flip(valor):
	$spr.flip_h=valor
	$hit.flip_h=valor
	if valor:
		$hit.position.x=-12
	else:
		$hit.position.x=12

func set_ruta(ruta_nueva):
	ruta=ruta_nueva

func hit():
	if get_global_mouse_position().x<global_position.x:
		flip(true)
	else:
		flip(false)

	$anim.play("hit")
