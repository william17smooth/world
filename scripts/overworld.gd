extends Node2D

const ARBOL=0
const ROCA=1

var pathfinder
var destino=Vector2.ZERO

var ruta=[]


func _ready():
	pathfinder=$TileMapPathfinder
	pathfinder.agregar_zona(0,0,15,16)

func _process(delta):
	if Input.is_action_just_pressed("click_izq"):
		var origen=$YSort/jugador.global_position
		origen=$terreno.world_to_map(origen)
	
		var nuevo_destino=$terreno.world_to_map(get_global_mouse_position())
		destino=nuevo_destino
		ruta=pathfinder.obtener_ruta(origen,destino)
		print(ruta)
		
		if ruta.size()>1 :
			
			if $YSort/jugador.ruta.size()>0: 
				if Array(ruta).back()!=Array($YSort/jugador.ruta).back():
					$YSort/jugador.ruta=ruta
			else:
				$YSort/jugador.ruta=ruta
		elif $YSort/jugador.puedo_mover and ruta.size()<1:
			
			if (destino.x>=origen.x-1) and (destino.x<=origen.x+1) and (destino.y>=origen.y-1) and (destino.y<=origen.y+1): 
				
				match $YSort/bloques.get_cellv(destino):
					ARBOL:
						print("tocaste arbol")
						generar_particula(destino)
					ROCA:
						print("tocaste piedra")
						generar_particula(destino)

func generar_particula(pos):
	$YSort/jugador.hit()
