extends Node

export var diagonales:bool

export (NodePath) var tilemap 
var mapa:TileMap
var id=0
var mapa_puntos=[]
export (Array,int) var tiles_transitables=[]

var pathfind=AStar2D.new()

func _ready():
	if tilemap=="":
		push_error("asigna un TileMap")
	else:
		mapa=get_node(tilemap)



func obtener_ruta(desde:Vector2,hasta:Vector2):
	if existe(hasta):
		return pathfind.get_point_path(get_id(desde),get_id(hasta))
	else:
		return []
func agregar_tile_transitable(id_tile:int):
	tiles_transitables.append(id_tile)

func agregar_zona(x,y,w,h):
	for i in range(x,w+1):
		for j in range(y,h+1):
			var celda=Vector2(i,j)
			pathfind.add_point(id,celda)
			mapa_puntos.append(celda)
			id+=1
	
	for i in range(x,w+1):
		for j in range(y,h+1):
			var celda=Vector2(i,j)
			if es_transitable(celda):
				if es_transitable(celda+Vector2.UP):
					pathfind.connect_points(get_id(celda),get_id(celda+Vector2.UP))
				if es_transitable(celda+Vector2.DOWN):
					pathfind.connect_points(get_id(celda),get_id(celda+Vector2.DOWN))
				if es_transitable(celda+Vector2.RIGHT):
					pathfind.connect_points(get_id(celda),get_id(celda+Vector2.RIGHT))
				if es_transitable(celda+Vector2.LEFT):
					pathfind.connect_points(get_id(celda),get_id(celda+Vector2.LEFT))
				
				if diagonales:
					if es_transitable(celda+Vector2.UP+Vector2.RIGHT) and es_transitable(celda+Vector2.RIGHT) and es_transitable(celda+Vector2.UP):
						pathfind.connect_points(get_id(celda),get_id(celda+Vector2.RIGHT+Vector2.UP))
					if es_transitable(celda+Vector2.UP+Vector2.LEFT) and es_transitable(celda+Vector2.LEFT) and es_transitable(celda+Vector2.UP):
						pathfind.connect_points(get_id(celda),get_id(celda+Vector2.LEFT+Vector2.UP))
					
					if es_transitable(celda+Vector2.DOWN+Vector2.RIGHT) and es_transitable(celda+Vector2.RIGHT) and es_transitable(celda+Vector2.DOWN):
						pathfind.connect_points(get_id(celda),get_id(celda+Vector2.RIGHT+Vector2.DOWN))
					if es_transitable(celda+Vector2.DOWN+Vector2.LEFT) and es_transitable(celda+Vector2.LEFT) and es_transitable(celda+Vector2.DOWN):
						pathfind.connect_points(get_id(celda),get_id(celda+Vector2.LEFT+Vector2.DOWN))
				
func existe(point:Vector2):
	if mapa_puntos.find(point)!=-1:
		return true
	else:
		return false

func get_id(point:Vector2):
	return mapa_puntos.find(point)

func es_transitable(tile:Vector2):
	if existe(tile):
		for i in tiles_transitables:
			if mapa.get_cellv(tile)==i:
				return true
	return false
	
