tool
extends EditorPlugin

func _enter_tree():
	add_custom_type("TileMapPathfinder","Node",preload("res://addons/Tilemap Pathfinder/pathfinder.gd"),preload("res://addons/Tilemap Pathfinder/icon.png"))

func _exit_tree():
	remove_custom_type("TileMap Pathfinder")
